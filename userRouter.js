//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');
const { request } = require('https');

const router = express.Router();

//Import Company Data
const { userListArray, userListObject } = require("./data.js");

router.get("/users", (request, response) => {

    let user = request.query.user;
    if (user) {
        //Khởi tạo biến companyListResponse là kết quả tìm được 
        let userListObject = [];

        //Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
        for (let index = 0; index < userListArray.length; index++) {
            let userElement = userListArray[index];

            if (userElement.checkUserById(id)) {
                userListObject.push(userElement);
            }
        }
        response.status(200).json({
            user: userListObject
        })
    } else {
        response.status(200).json({
            user: userListObject
        })

    }
});

router.get("/users/:userId", (request, response) => {
    let userId = request.params.id;

    response.json({
        message: "GET user Id = " + userId
    })

})

router.post("/users", (request, response) => {
    response.json({
        message: "POST All Users"
    })
});

router.put("/users", (request, response) => {
    response.json({
        message: "PUT All Users"
    });
});

router.delete("/users", (request, response) => {
    response.json({
        message: "DELETE All Users"
    });
});

//Export dữ liệu thành 1 module
module.exports = router;
