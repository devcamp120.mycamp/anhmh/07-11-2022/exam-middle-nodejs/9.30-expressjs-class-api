//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const { response } = require("express");
const express = require("express");
const { request } = require("http");
const { userListArray, userListObject } = require("./data.js");

//Import Router
const userRouter = require("./userRouter");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    response.status(200).json({
        message: "User API"
    })
})

app.get("/users-object", (request, response) => {
    let user = request.query.user;
    if (user) {
        //Khởi tạo biến userListResponse là kết quả tìm được 
        let userListObject = [];

        //Duyệt phần tử của mảng để tìm được user có Id tương ứng
        for (let index = 0; index < userListArray.length; index++) {
            let userElement = userListArray[index];

            if (userElement.checkUserById(id)) {
                userListObject.push(userElement);
            }
        }
        response.status(200).json({
            user: userListObject
        })
    } else {
        response.status(200).json({
            user: userListArray
        })  

    }
})

app.use("/", userRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})