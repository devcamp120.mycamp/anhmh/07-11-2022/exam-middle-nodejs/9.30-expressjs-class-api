const { runInThisContext } = require("vm");

//Tạo một class User
class User {
    //Hàm tạo Constructor
    constructor(paramId, paramName, paramPosition, paramOffice, paramAge, paramStartDate) {
        this.id = paramId;
        this.name = paramName;
        this.position = paramPosition;
        this.office = paramOffice;
        this.age = paramAge;
        this.startDate = paramStartDate;
    }
    //Hàm kiểm tra tên User có chứa Id được truyền vào hay không
    checkUserById(paramId) {
        return this.id === paramId
    } 
}

let userListArray = [];

//Khởi tạo các class User
let user_1 = new User(1, "Airi Satou", "Accountant", "Tokyo", "33", "2008/11/28");
userListArray.push(user_1);


let user_2 = new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "47", "2009/10/09");
userListArray.push(user_2);

let user_3 = new User(3, "Ashton Cox", "Junior Technical Author", "London", "66", "2009/01/12");
userListArray.push(user_3);

let user_4 = new User(4, "Bradley Greer", "Software Engineer", "London", "41", "2012/10/13");
userListArray.push(user_4);

let user_5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", "28", "2011/06/07");
userListArray.push(user_5);

let user_6 = new User(6, "Brielle Williamson", "Integration Specialist", "New York", "61", "2012/12/02");
userListArray.push(user_6);

let user_7 = new User(7, "Bruno Nash", "Software Engineer", "London", "38", "2011/05/03");
userListArray.push(user_7);

let user_8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", "21", "2011/12/12");
userListArray.push(user_8);

let user_9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", "46","2011/12/06");
userListArray.push(user_9);

let user_10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "22", "2012/03/29");
userListArray.push(user_10);


var userListObject = [
    {
        id: 1,
        name: "Airi Satou",
        position: "Accountant",
        office: "Tokyo",
        age: "33",
        startDate: "2008/11/28"
    },
    {
        id: 2,
        name: "Angelica Ramos",
        position: "Chief Executive Officer (CEO)",
        office: "London",
        age: "47",
        startDate: "2009/10/09"
        
    },
    {
        id: 3,
        name: "Ashton Cox",
        position: "Junior Technical Author",
        office: "San Francisco",
        age: "66",
        startDate: "2009/01/12"
        
    },
    {
        id: 4,
        name: "Bradley Greer",
        position: "Software Engineer",
        office: "London",
        age: "41",
        startDate: "2012/10/13"
    },
    {
        id: 5,
        name: "Brenden Wagner",
        position: "Software Engineer",
        office: "San Francisco",
        age: "28",
        startDate: "2011/06/07"      
    },
    {
        id: 6,
        name: "Brielle Williamson",
        position: "Integration Specialist",
        office: "New York",
        age: "61",
        startDate: "2012/12/02"
        
    },
    {
        id: 7,
        name: "Bruno Nash",
        position: "Software Engineer",
        office: "London",
        age: "38",
        startDate: "2011/05/03"
        
    },
    {
        id: 8,
        name: "Caesar Vance",
        position: "Pre-Sales Support",
        office: "New York",
        age: "21",
        startDate: "2011/12/12"
        
    },
    {
        id: 9,
        name: "Cara Stevens",
        position: "Sales Assistant",
        office: "New York",
        age: "46",
        startDate: "2011/12/06"
        
    },
    {
        id: 10,
        name: "Cedric Kelly",
        position: "Senior Javascript Developer",
        office: "Edinburgh",
        age: "22",
        startDate: "2012/03/29"
    }
]

module.exports = { userListArray, userListObject }